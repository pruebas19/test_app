import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HomePage } from './home.page';

describe('HomePage', () => {
  let component: HomePage;
  let fixture: ComponentFixture<HomePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HomePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('Init: should load more than one item', () => {
    component.ngOnInit();

    expect(component.items.length).toBeGreaterThanOrEqual(1);
  });

  it('Filter a string and get less than 4000', () => {
    component.ngOnInit();

    component.search({ detail: {value: 'tu'}});

    expect(component.items.length).toBeLessThan(4000);
  });
});
