import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ImageItem } from '../../model/image.interface';
import { PhotoService } from '../../services/photo.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit, OnDestroy {

  items: ImageItem[] = [];
  photoSubscription: Subscription;

  constructor(
    public photoSvc: PhotoService
  ) {}

  ngOnInit() {
    const obs = this.photoSvc.loadRandomJson();
    this.photoSubscription = obs
    .subscribe(
      data => {
        this.items = data;
      },
      error => {
        console.log('Error al cargar items: ', error);
      }
    );
  }

  ngOnDestroy(): void {
    if (this.photoSubscription) {
      this.photoSubscription.unsubscribe();
    }
  }

  search(evt) {
    const term = evt.detail.value;
    this.photoSvc.filterJson(term);
  }
}
