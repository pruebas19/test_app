import { Component, Input } from '@angular/core';
import { ImageItem } from '../../model/image.interface';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.scss'],
})
export class ImageComponent {

  @Input() item: ImageItem;

  imgError(): void {
    this.item.photo = '/assets/icon/favicon.png';
  }
}
