import { Component, Input } from '@angular/core';
import { ImageItem } from '../../model/image.interface';

@Component({
  selector: 'app-image-list',
  templateUrl: './image-list.component.html',
  styleUrls: ['./image-list.component.scss'],
})
export class ImageListComponent {

  @Input() items: ImageItem[] = [];

}
