import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { LoremIpsum } from 'lorem-ipsum';
import { ImageItem } from '../model/image.interface';

@Injectable({
  providedIn: 'root'
})
export class PhotoService {

  private readonly SIZE_ARRAY = 4000;
  private lorem: LoremIpsum;
  private arrayBase: ImageItem[] = [];

  private itemsSubject: BehaviorSubject<ImageItem[]> = new BehaviorSubject<ImageItem[]>([]);
  public items$: Observable<ImageItem[]> = this.itemsSubject.asObservable();

  constructor() {
    this.lorem = new LoremIpsum({
      sentencesPerParagraph: {
        max: 8,
        min: 4
      },
      wordsPerSentence: {
        max: 16,
        min: 4
      }
    });
  }

  loadRandomJson(): Observable<ImageItem[]> {
    const res = [];
    for (let i = 1; i <= this.SIZE_ARRAY; i++) {
      const id = i;
      const photo = `https://picsum.photos/id/${i}/500/500`;
      const text = this.lorem.generateWords(7);
      res.push({
        id,
        photo,
        text
      });
    }
    console.log(res);
    this.arrayBase = res;
    this.itemsSubject.next(this.arrayBase);
    return this.items$;
  }

  filterJson(term: string): void {
    if (term !== '') {
      const filter = this.arrayBase.filter(i => {
        return (i.id === +term || i.text.includes(term));
      });
      this.itemsSubject.next(filter);
    } else {
      this.itemsSubject.next(this.arrayBase);
    }
  }
}
