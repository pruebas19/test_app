import { TestBed } from '@angular/core/testing';

import { PhotoService } from './photo.service';

describe('RandomJsonService', () => {
  let service: PhotoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PhotoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should items$ next a a random json array with 4000 items' , () => {

    let items = [];

    service.items$.subscribe(
      data => {
        items = data;
      }
    );

    service.loadRandomJson();

    expect(items.length).toBe(4000);
  });

  it('should filter one item with id 320', () => {
    let items = [];

    service.items$.subscribe(
      data => {
        items = data;
      }
    );

    service.loadRandomJson();

    service.filterJson('320');

    expect(items.length).toBe(1);
  });
});
