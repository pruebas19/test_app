import { AppPage } from './app.po';

describe('new App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('title should be prueba filtrado', () => {
    page.navigateTo();
    expect(page.getPageTitle()).toContain('Prueba Filtrado');
  });
});
